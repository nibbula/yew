;;;
;;; opsys-config.lisp - Configuration options for OPSYS
;;;

(defpackage :opsys-config
  (:documentation "Configuration options for OPSYS")
  (:use :cl :config)
  (:export
   #:*config*
   #:*configuration*
   ))
(in-package :opsys-config)

(defun c-library-vendor ()
  #+linux
  ;; A bunch of junk for trying to determine which libc we are running with on
  ;; linux, BUT we have to do it without dlib, opsys, and glob since we want to
  ;; be able to do it *before* loading opsys.
  ;;
  ;; Also cl:directory doesn't work consistently for symlinks, so we have to do
  ;; the harder thing of reading the stupid "maps" file instead of the
  ;; "maps_files" directory.

  (labels ((ssplit (s char)
             "Simple split ‘s’ by ‘char’."
             (let* ((start 0) pos
                    (result
                      (loop :while (setf pos (position char s :start start))
                            :when (> (- pos start) 1)
                            :collect (subseq s start pos)
                            :end
                            :do (setf start (1+ pos)))))
               (if (< start (length s))
                   (append result (list (subseq s start)))
                   result)))
           (fake-match (pattern item)
             "Match a pattern with * wildcards."
             (let ((pieces (ssplit pattern #\*)))
               (loop :with start = 0 :and pos
                     :for p :in pieces :do
                        (when (not (setf pos (search p item :start2 start)))
                          (return nil))
                        (setf start (+ pos (length p)))
                     :finally (return t))))
           ;; @@@ This could be useful in normal opsys, but we'd have to
           ;; do it other operating systems.
           (mapped-files ()
             "Return a list of mapped files in our current image."
             (flet ((lines (file)
                      "Return a list of lines from ‘file’."
                      (with-open-file (f file :direction :input)
                        (loop :with l
                              :while (setf l (read-line f nil))
                              :collect l))))
               (remove-duplicates
                (mapcar (lambda (_) (nth 5 _))
                        (remove-if-not
                         (lambda (_)
                           (and (= (length _) 6)
                                (char= (char (nth 5 _) 0) #\/)))
                         (mapcar (lambda (_) (ssplit _ #\space))
                                 (lines "/proc/self/maps"))))
                :test #'equal))))
    (let ((libs (mapped-files)))
      (flet ((match (m)
               (find-if (lambda (_) (fake-match m _)) libs)))
        (cond
          ((match "*gnu*libc.so*")  :gnu)
          ((match "*musl*.so*")     :musl)
          (t                        :unknown)))))
  #+darwin :apple
  #+windows :ms
  #+(or openbsd freebsd netbsd) :bsd ;; @@@ maybe separate?
  #-(or linux darwin windows openbsd freebsd netbsd) :unknown)

(defconfiguration
  ((optimization-settings list
    "Default optimization settings for each file/compilation unit?."
    ;; If we don't have at least debug 2, then most compilers won't save
    ;; the function arguments.
    `((debug 2)))
   (c-library-vendor keyword
    "A keyword representing the C library type. Like :gnu, :musl etc.
or :unknown if we can't figure it out."
    (c-library-vendor))))

(configure)

;; fuxord because unicode uses dlib
#| 
;; Since we want people to be able to use this thing without depending on our
;; sprawling monorepo, where the "config" package currently resides,
;; FAKE IT:

(let ((fake-it (not (asdf:locate-system :dlib))))
  (defvar *config* `(:optimization-settings ((debug 2))
		     :fake-dlib ,fake-it))
  (when fake-it
    (pushnew :use-fake-dlib *features*)))
|#

;; End
