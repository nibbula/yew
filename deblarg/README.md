This is a run down debugger that the yokels who hang about [town][1] in the
[shell][2] are always getting un-ceremoniously tossed into. I'm sure you
decent city folk would likely be better served by a right fancy debugger in
[Emacs][3] or [something][4].

[1]: https://codeberg.org/nibbula/yew/
[2]: https://codeberg.org/nibbula/yew/src/branch/master/lish
[3]: https://github.com/slime/slime
[4]: https://github.com/joaotavora/sly

*Note* that this depends on [yew](https://codeberg.org/nibbula/yew/).
