;;;
;;; unipose.lisp - Compose unicode characters
;;;

(defpackage :unipose
  (:documentation "Compose unicode characters.")
  (:use :cl :dlib)
  (:export
   #:unipose
   #:set-unipose
   #:save-unipose-for-emacs
   ))
(in-package :unipose)

;; I know this is a strange madness that most people won't want.
;; - It should really be made customizable.
;; - It should load *unipose* from $HOME/.unipose or something.
;; - It should have an "add your own" function.
;; - It should have a "save it" function.
;; - It should have a set which is RFC 1345
;; - It should handle arbitrary length / levels

(eval-when (:compile-toplevel :load-toplevel :execute)
  (locally #+sbcl (declare (sb-ext:muffle-conditions sb-ext:compiler-note))
    (cond
      ((<= char-code-limit #xff)
       (warn "Unipose isn't going to work for 8 bit characters.")
       (d-add-feature :t-8))
      ((<= char-code-limit #x10000)
       (warn "Some unipose sequences won't work with 16 bit characters.")
       (d-add-feature :t-16))
      ((< char-code-limit #x110000)
       (warn "You might be missing some characters.")
       (d-add-feature :t-shrunk)))))

;; Slow scrollin' pardner.
;; Of course pairs are more efficient, but it's easier to edit without dots.
;; This can be considered as just a tree of alists except that after the first
;; level a list in the ‘car’ is considered as alternatives.
(defparameter *unipose*
  #-t-8
  '((#\0 ((#\^ #\⁰)))
    (#\1 ((#\2 #\½) (#\4 #\¼) (#\^ #\¹) (#\+ #\†)))
    (#\2 ((#\^ #\²) (#\+ #\‡)))
    (#\3 ((#\^ #\³) (#\4 #\¾)))
    (#\4 ((#\^ #\⁴)))
    (#\5 ((#\^ #\⁵)))
    (#\6 ((#\^ #\⁶)))
    (#\7 ((#\^ #\⁷)))
    (#\8 ((#\8 #\∞) (#\^ #\⁸)))
    (#\9 ((#\^ #\⁹)))
    (#\A ((#\^ #\Â) (#\' #\Á) (#\` #\À) (#\" #\Ä) (#\E #\Æ) (#\o #\Å) (#\~ #\Ã)
	  #-t-16 (#\B #\🆎)
	  ))
    (#\a ((#\^ #\â) (#\' #\á) (#\` #\à) (#\" #\ä) (#\e #\æ) (#\o #\å) (#\~ #\ã)
	  (#\_ #\ª) (#\p #\) (#\< #\←) (#\> #\→) (#\= #\↔)))
    (#\B ((#\B #\ß)))
    (#\b (#-t-16 (#\u ((#\g #\🐛)))))
    (#\c (((#\0 #\O) #\©) ((#\/ #\|) #\¢) (#\, #\ç) (#\* #\☪) (#\C #\￠)
          #-t-16 (#\o ((#\w #\🐄))) #-t-16 (#\a ((#\t #\🐈)))))
    (#\C (((#\0 #\O) #\©) ((#\/ #\|) #\￠) (#\, #\Ç) (#\* #\☪)
	  #-t-16 (#\L #\🆑)))
    (#\D ((#\D #\∆) (#\- #\Ð)))
    (#\d ((#\g #\˚) #-t-16 (#\o ((#\g #\🐕)))))
    (#\E ((#\^ #\Ê) (#\' #\É) (#\` #\È) (#\" #\Ë) ((#\~ #\- #\=) #\€)))
    (#\f ((#\~ #\ƒ) #| (#\f #\ﬀ) |# (#\i #\ﬁ) (#\l #\ﬂ) (#\t #\ﬅ)
          (#\f ((#\f #\ﬀ) (#\i #\ﬃ) (#\l #\ﬄ)))))
    (#\e ((#\^ #\ê) (#\' #\é) (#\` #\è) (#\" #\ë) ((#\~ #\- #\=) #\€)))
    (#\I ((#\^ #\Î) (#\' #\Í) (#\` #\Ì) (#\" #\Ï) #-t-16 (#\D #\🆔)))
    (#\i ((#\^ #\î) (#\' #\í) (#\` #\ì) (#\" #\ï) (#\j #\ĳ)))
    (#\L ((#\L #\Λ) (#\- #\£) (#\/ #\Ł)))
    (#\l ((#\l #\λ) (#\/ #\ł)))
    (#\M #-t-16 ((#\U #\🈚)))
    (#\m ((#\u #\µ)))
    (#\N ((#\~ #\Ñ) #-t-16 (#\G #\🆖) (#\o #\№)))
    (#\n ((#\~ #\ñ) (#\^ #\ⁿ)))
    (#\O ((#\^ #\Ô) (#\' #\Ó) (#\` #\Ò) (#\" #\Ö) (#\E #\Œ) (#\~ #\Õ) (#\O #\Ω)
	  #-t-16 (#\K #\🆗)))
    (#\o ((#\^ #\ô) (#\' #\ó) (#\` #\ò) (#\" #\ö) (#\e #\œ) (#\~ #\õ) (#\_ #\º)))
    (#\P ((#\H #\Φ) (#\I #\Π) (#\P #\¶)))
    (#\p ((#\h #\φ) (#\i #\π)))
    (#\r (((#\0 #\O #\o) #\®)))
    (#\S ((#\S #\∑)))
    (#\s ((#\e #\§) (#\r #\√) (#\s #\ß) (#\t #\ﬆ)))
    (#\T (((#\M #\m) #\™) (#\T #\Þ)))
    (#\t (((#\M #\m) #\™) (#\T #\þ)))
    (#\U ((#\^ #\Û) (#\' #\Ú) (#\` #\Ù) (#\" #\Ü)))
    (#\u ((#\^ #\û) (#\' #\ú) (#\` #\ù) (#\" #\ü)))
    (#\V #-t-16 ((#\S #\🆚)))
    (#\x ((#\x #\×) (#\* #\※)))
    (#\Y ((#\- #\¥) (#\' #\Ý) (#\" #\Ÿ)))
    (#\y ((#\- #\¥) (#\' #\ý) (#\" #\ÿ)))
    (#\^ (;; captial circumflex
	  (#\A #\Â) (#\C #\Ĉ) (#\E #\Ê) (#\G #\Ĝ) (#\H #\Ĥ) (#\I #\Î) (#\J #\Ĵ)
	  (#\O #\Ô) (#\S #\Ŝ) (#\U #\Û) (#\W #\Ŵ) (#\Y #\Ŷ)
	  ;; lower circumflex
	  (#\a #\â) (#\c #\ĉ) (#\e #\ê) (#\g #\ĝ) (#\h #\ĥ) (#\i #\î) (#\j #\ĵ)
	  (#\o #\ô) (#\s #\ŝ) (#\u #\û) (#\w #\ŵ) (#\y #\ŷ)
          ;; superscripts
          (#\0 #\⁰) (#\1 #\¹) (#\2 #\²) (#\3 #\³) (#\4 #\⁴) (#\5 #\⁵) (#\6 #\⁶)
          (#\7 #\⁷) (#\8 #\⁸) (#\9 #\⁹) (#\+ #\⁺) (#\- #\⁻)
          ;; misc
          (#\space #\^)
          (#\^
           ;; carons
           ;; uppercase
           ((#\A #\Ǎ) (#\C #\Č) (#\E #\Ě) (#\G #\Ǧ) (#\I #\Ǐ) (#\K #\Ǩ)
            (#\N #\Ň) (#\O #\Ǒ) (#\R #\Ř) (#\S #\Š) (#\U #\Ǔ) (#\Z #\Ž)
            ;; lowercase
            (#\a #\ǎ) (#\c #\č)(#\e #\ě) (#\g #\ǧ) (#\i #\ǐ) (#\k #\ǩ)
            (#\n #\ň) (#\o #\ǒ) (#\r #\ř) (#\s #\š) (#\u #\ǔ) (#\z #\ž)))))
    (#\' (;; capital acute
	  (#\A #\Á) (#\C #\Ć) (#\E #\É) (#\I #\Í) (#\N #\Ń) (#\O #\Ó) (#\S #\Ś)
          (#\U #\Ú) (#\W #\Ẃ) (#\Y #\Ý) (#\Z #\Ź)
	  ;; lower acute
	  (#\a #\á) (#\c #\ć) (#\e #\é) (#\i #\í) (#\n #\ń) (#\o #\ó) (#\s #\ś)
          (#\u #\ú) (#\w #\ẃ) (#\y #\ý) (#\z #\ź)
	  ;; misc acute
	  (#\' #\´) (#\< #\‘) (#\> #\’) (#\_ #\́)
          (#\space #\')))
    (#\` (;; capital grave
          (#\A #\À) (#\E #\È) (#\I #\Ì) (#\O #\Ò) (#\U #\Ù) (#\W #\Ẁ) (#\Y #\Ỳ)
	  ;; lower greve
	  (#\a #\à) (#\e #\è) (#\i #\ì) (#\o #\ò) (#\u #\ù) (#\w #\ẁ) (#\y #\ỳ)
	  ;; misc grave
	  (#\_ #\̀) (#\space #\`)))
    (#\" (;; capital diaeresis
	  (#\A #\Ä) (#\E #\Ë) (#\I #\Ï) (#\O #\Ö) (#\U #\Ü) (#\W #\Ẅ) (#\Y #\Ÿ)
	  ;; lower diaeresis
	  (#\a #\ä) (#\e #\ë) (#\i #\ï) (#\o #\ö) (#\u #\ü) (#\w #\ẅ) (#\y #\ÿ)
	  ;; misc diaeresis
	  (#\_ #\̈) (#\" #\¨) (#\v #\„) (#\< #\“) (#\> #\”) (#\s #\ß)
          (#\space #\")))
    (#\~ (;; upper twiddles
	 (#\A #\Ã) (#\C #\Ç) (#\D #\Ð) (#\G #\Ğ) (#\O #\Õ) (#\N #\Ñ) (#\T #\Þ)
         (#\U #\Ŭ)
	 ;; lower twiddles
	 (#\a #\ã) (#\c #\ç) (#\d #\ð) (#\g #\ğ) (#\o #\õ) (#\n #\ñ) (#\t #\þ)
         (#\u #\ŭ)
	 ;; misc twiddles
	 (#\e #\€) (#\p #\¶) (#\s #\§) (#\u #\µ) (#\x #\¤) (#\? #\¿) (#\! #\¡)
	 (#\$ #\£) (#\. #\·) (#\< #\«) (#\> #\») (#\~ #\¬) (#\= #\≈)
         (#\space #\~)))
    (#\! ((#\! #\‼) (#\v #\¡) (#\? #\⁉) (#\/ #\❢)))
    (#\? ((#\v #\¿) (#\? #\⁇) (#\! #\⁈) ((#\| #\/) #\‽)))
    (#\/ ((#\A #\Å) (#\E #\Æ) (#\L #\Ł) (#\O #\Ø)
          (#\a #\å) (#\e #\æ) (#\c #\¢) (#\l #\ł) (#\o #\ø)
          (#\/ #\÷) (#\= #\≠)))
    (#\| ((#\| #\¦)))
    (#\+ ((#\- #\±) (#\+ #\†) (#\^ #\⁺)))
    (#\, ((#\A #\Ą) (#\C #\Ç) (#\N #\Ņ) (#\S #\Ş)
          (#\a #\ą) (#\c #\ç) (#\n #\ņ) (#\s #\ş)
          (#\, #\¸)))
    (#\- ((#\+ #\±) (#\: #\÷) (#\> #\→) (#\^ #\⁻) (#\- #\—) (#\~ #\〜)
          (#\? #\­)))
    (#\= ((#\A #\Ā) (#\E #\Ē) (#\G #\Ḡ) (#\I #\Ī) (#\O #\Ō) (#\U #\Ū) (#\Y #\Ȳ)
          (#\a #\ā) (#\e #\ē) (#\g #\ḡ) (#\i #\ī) (#\o #\ō) (#\u #\ū) (#\y #\ȳ)
          (#\/ ((#\/ #\≠) (#\E #\Ǣ) (#\e #\ǣ)))
          (#\~ #\≈) (#\> #\⇒) (#\^ #\⁼) (#\= #\¯)))
    (#\_ (;; combining characters
	  (#\^ #\̂) (#\` #\̀) (#\' #\́) (#\~ #\̃) (#\- #\̄) (#\_ #\̲)
	  (#\O #\⃝) (#\[ #\⃞) (#\< #\⃟)
          ;; subscripts
          (#\0 #\₀) (#\1 #\₁) (#\2 #\₂) (#\3 #\₃) (#\4 #\₄) (#\5 #\₅) (#\6 #\₆)
          (#\7 #\₇) (#\8 #\₈) (#\9 #\₉) (#\+ #\₊) (#\- #\₋)
	  ;; misc underscore
	  (#\^ #\¯) (#\a #\ª) (#\o #\º) (#\< #\≤) (#\> #\≥) (#\- #\−)))
    (#\< ((#\= #\≤) (#\< #\«)))
    (#\> ((#\= #\≥) (#\> #\»)))
    (#\( ((#\^ #\⁽) (#\( #\⸨)))
    (#\) ((#\^ #\⁾) (#\) #\⸩)))
    (#\* ((#\space #.(code-char 160)) (#\! #\¡) (#\" #\¨) (#\$ #\¤) (#\+ #\±)
          (#\- #\­) (#\. #\·) (#\< #\«) (#\> #\») (#\= #\¯) (#\? #\¿) (#\C #\©)
          (#\E #\€) (#\L #\£) (#\P #\¶) (#\R #\®) (#\S #\§) (#\T #\™) (#\Y #\¥)
          (#\c #\¢) (#\o #\°) ((#\u #\m) #\μ) (#\x #\×) (#\| #\¦) (#\* #\•)
          (#\' #\′) (#\" #\″)))
    (#\: ((#\- #\÷)))
    (#\. ((#\o #\•) (#\space #\·) (#\. #\…) (#\- #\⋅) (#\^ #\˚)
          (#\Z #\ż)))
    (#\$ ((#\$ #\¤)))
    (#\% ((#\% #\‰))))
  "Unicode compose character lists.")

;; Since this is probably just for me, does it really matter? 
(defun save-unipose-for-emacs (&optional (file "~/src/el/unipose-data.el"))
  "Save the unipose date as loadable ELisp in ‘file’."
  (with-open-file (stream (glob:expand-tilde file)
			  :direction :output
			  :if-exists :supersede)
    (format stream ";;;~%;;; unipose-data.el~%;;;~%~@
		    ;;; Automatically generated from UNIPOSE at ~a~%~%"
	    (dtime:date-string))
    (print-unipose-for-emacs :stream stream)
    (format stream "~%;; End~%")))

(defun print-unipose-for-emacs (&key (tree *unipose*)
				  (stream *standard-output*) (depth 0) (n 0))
  "Print the unipose list for loading into emacs."
  (if (= depth 0)
      (progn
	(format stream "(setq *unipose*~%  '(")
	(loop
	   :for branch :in tree
	   :for n = 0 :then (+ n 1)
	   :do
	   (when (> n 0)
	     (write-string "   " stream))
	   (incf depth)
	   (print-unipose-for-emacs :tree branch :stream stream
				    :depth depth :n n)
	   (terpri stream))
	(format stream "   ))~%"))
      (progn
	(incf depth)
	(typecase tree
	  (null
	   (write-char #\) stream))
	  (list
	   (incf depth)
	   (when (and (> n 0) (> depth 1))
	     (write-char #\space stream))
	   (write-char #\( stream)
	   (loop
	      :for l :in tree
	      :for n = 0 :then (+ n 1)
	      :do
	      (print-unipose-for-emacs :tree l :stream stream
				       :depth depth :n n))
	   (write-char #\) stream))
	  (character
	   (when (> n 0)
	     (write-char #\space stream))
	   (write-char #\? stream)
	   (when (char= tree #\")
	     (write-char #\\ stream))
	   (write-char tree stream))
	  (t
	   (error "Unknown object type in unipose data: ~s ~s~%"
		  (type-of tree) tree))))))

(defun old-unipose (first-char second-char)
  "Return the character composed from FIRST-CHAR and SECOND-CHAR, or NIL if
there is none defined."
  (let ((level2 (cadr (assoc first-char *unipose*))))
    (or
     (dolist (level3 level2)
       (if (and (listp (car level3)) (position second-char (car level3)))
	   (return (cadr level3))
	   (when (eq (car level3) second-char)
	     (return (cadr level3)))))
     nil)))

(defun unipose (char-list &key completions)
  "Return the character composed from ‘char-list’, NIL if there is none defined,
or T if there are more levels."
  (let ((u *unipose*)
        (cc char-list)
        c match)
    (loop
      :while cc
      :do
      (setf c (car cc))
      (when (not (setf match
                       (assoc c u :test
                              ;; Check for multiple prefix to same suffix
                              (lambda (a b)
                                (if (consp b)
                                    (find a b)
                                    (eql a b))))))
        (return-from unipose (if completions u nil)))
      (setf u (cadr match) #| expecting nil |#)
      (cond
        ((null u)
         ;; This is actually malformed data, so maybe error?
         (return nil))
        ((consp u)
         ;; Go down another level
         )
        (t
         ;; It should be a result thing, but return NIL if we didn't match
         ;; everything in ‘char-list’.
         (return-from unipose (if (cdr cc) nil u))))
      (setf cc (cdr cc)))
    (if completions u t)))

;; This assumes the default value of *unipose*
(defun test-unipose ()
  (let (result)
    (dolist (e '((eq t (unipose '()))
                 (eq t (unipose '(#\e)))
                 (eq #\é (unipose '(#\e #\')))
                 (eq nil (unipose '(#\e #\' #\x)))
                 (eq t (unipose '(#\f #\f)))
                 (eq #\ﬀ (unipose '(#\f #\f #\f)))
                 (eq #\ﬃ (unipose '(#\f #\f #\i)))
                 (eq nil (unipose '(#\f #\f #\i #\z)))
                 ))
      (when (not (setf result (eval e)))
        (format t "FAIL: ~s ~s~%" result e)))
    (format t "OK~%")))

;; @@@ This needs testing!! Don't use it yet!
#+(or)
(defun set-unipose (first-char second-char result)
  "Add a character composition to unipose."
  (let ((level2 (cadr (assoc first-char *unipose*))))
    (if (null level2)
	;; If there' not top level first-char, add it
	(setf *unipose* (acons first-char
			       (list (list second-char result)) *unipose*))
	;; Look at sub levels
	(dolist (level3 level2)
	  (if (and (listp (car level3)) (position second-char (car level3)))
	      ;; If multiple second-chars map to the same result
	      (when (not (eq (cadr level3) result))
		;; If the result is not the same, make a new pair
		(setf level3
		      (cons level3 (list (list second-char result)))))
	      ;; If it's a one to one mapping, add
	      (when (eq (car level3) second-char)
		(return (setf (cadr level3) result))))))
    nil))

;; Clean up
#+t-8      (d-remove-feature :t-8)
#+t-16     (d-remove-feature :t-16)
#+t-shrunk (d-remove-feature :t-shrunk)

;; End
