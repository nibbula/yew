;;;								-*- Lisp -*-
;;; dbase64.asd - System definition for base64
;;;

(defsystem dbase64
  :name               "dbase64"
  :description        "Base64 encoding"
  :version            "0.1.0"
  :author             "Nibby Nebbulous <nibbula -(. @ .)- uucp!gmail.com>"
  :license            "GPL-3.0-only"
  :source-control     :git
  :long-description   "Base64 encoding and decoding."
  :depends-on (:dlib)
  :components
  ((:file "dbase64")))
