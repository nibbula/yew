;;;
;;; dbase64-test.lisp - Tests for Base64.
;;;

(defpackage :dbase64-test
  (:documentation "Tests for Base64.")
  (:use :cl :dbase64 :test :dlib)
  (:export
   #:run
   ))
(in-package :dbase64-test)

#|──────────────────────────────────────────────────────────────────────────┤#
 │ Encode
 ╰|#

(deftests (encode-test-1 :doc "Basic encoding tests from RFC4648.")
  (equal (encode-string "")       "")
  (equal (encode-string "f")      "Zg==")
  (equal (encode-string "fo")     "Zm8=")
  (equal (encode-string "foo")    "Zm9v")
  (equal (encode-string "foob")   "Zm9vYg==")
  (equal (encode-string "fooba")  "Zm9vYmE=")
  (equal (encode-string "foobar") "Zm9vYmFy"))

(defun ub8 (str)
  (make-array (length str) :element-type '(unsigned-byte 8)
              :initial-contents (map 'vector #'char-code str)))

(defun vec (str)
  (map 'vector #'char-code str))

(deftests (encode-test-2 :doc "encode unsigned byte")
  (equalp (encode-ub8 (ub8 ""))       (ub8 ""))
  (equalp (encode-ub8 (ub8 "f"))      (ub8 "Zg=="))
  (equalp (encode-ub8 (ub8 "fo"))     (ub8 "Zm8="))
  (equalp (encode-ub8 (ub8 "foo"))    (ub8 "Zm9v"))
  (equalp (encode-ub8 (ub8 "foob"))   (ub8 "Zm9vYg=="))
  (equalp (encode-ub8 (ub8 "fooba"))  (ub8 "Zm9vYmE="))
  (equalp (encode-ub8 (ub8 "foobar")) (ub8 "Zm9vYmFy"))
  )

(deftests (encode-test-3 :doc "encode unsigned byte to string")
  (equal (encode-ub8-to-string (ub8 ""))       "")
  (equal (encode-ub8-to-string (ub8 "f"))      "Zg==")
  (equal (encode-ub8-to-string (ub8 "fo"))     "Zm8=")
  (equal (encode-ub8-to-string (ub8 "foo"))    "Zm9v")
  (equal (encode-ub8-to-string (ub8 "foob"))   "Zm9vYg==")
  (equal (encode-ub8-to-string (ub8 "fooba"))  "Zm9vYmE=")
  (equal (encode-ub8-to-string (ub8 "foobar")) "Zm9vYmFy")
  )

(deftests (encode-test-4 :doc "encode unsigned byte to string")
  (equal (encode-vector-to-string (vec ""))       "")
  (equal (encode-vector-to-string (vec "f"))      "Zg==")
  (equal (encode-vector-to-string (vec "fo"))     "Zm8=")
  (equal (encode-vector-to-string (vec "foo"))    "Zm9v")
  (equal (encode-vector-to-string (vec "foob"))   "Zm9vYg==")
  (equal (encode-vector-to-string (vec "fooba"))  "Zm9vYmE=")
  (equal (encode-vector-to-string (vec "foobar")) "Zm9vYmFy")
  )

(defun flurp (s1 s2)
  (equal s2
    (with-output-to-string (*standard-output*)
      (with-input-from-string (is s1)
        (encode-char-stream is)))))

(defun flurp-o (s1 s2)
  (equal s2
    (with-output-to-string (out)
      (with-input-from-string (is s1)
        (encode-char-stream is :output out)))))

(deftests (encode-test-5 :doc "encode char stream")
  (flurp ""       "")
  (flurp "f"      "Zg==")
  (flurp "fo"     "Zm8=")
  (flurp "foo"    "Zm9v")
  (flurp "foob"   "Zm9vYg==")
  (flurp "fooba"  "Zm9vYmE=")
  (flurp "foobar" "Zm9vYmFy")
  (flurp-o ""       "")
  (flurp-o "f"      "Zg==")
  (flurp-o "fo"     "Zm8=")
  (flurp-o "foo"    "Zm9v")
  (flurp-o "foob"   "Zm9vYg==")
  (flurp-o "fooba"  "Zm9vYmE=")
  (flurp-o "foobar" "Zm9vYmFy")
  )

(deftests (encode-test-6 :doc "give error on too high chars")
  (got-condition (base64-error) (encode "░▒▓█"))
  (got-condition (base64-error) (encode #(#x25B2 #x25B6 #x25BC #x25C0))))

#|──────────────────────────────────────────────────────────────────────────┤#
 │ Decode
 ╰|#

(deftests (decode-test-1 :doc "Basic decoding tests from RFC4648.")
  (equal (decode-string "")         "")
  (equal (decode-string "Zg==")     "f")
  (equal (decode-string "Zm8=")     "fo")
  (equal (decode-string "Zm9v")     "foo")
  (equal (decode-string "Zm9vYg==") "foob")
  (equal (decode-string "Zm9vYmE=") "fooba")
  (equal (decode-string "Zm9vYmFy") "foobar")
  )

(deftests (decode-test-2 :doc "decode unsigned byte")
  (equalp (decode-ub8 (ub8 ""))         (ub8 ""))
  (equalp (decode-ub8 (ub8 "Zg=="))     (ub8 "f"))
  (equalp (decode-ub8 (ub8 "Zm8="))     (ub8 "fo"))
  (equalp (decode-ub8 (ub8 "Zm9v"))     (ub8 "foo"))
  (equalp (decode-ub8 (ub8 "Zm9vYg==")) (ub8 "foob"))
  (equalp (decode-ub8 (ub8 "Zm9vYmE=")) (ub8 "fooba"))
  (equalp (decode-ub8 (ub8 "Zm9vYmFy")) (ub8 "foobar"))
  )

(deftests (decode-test-3 :doc "decode string to unsigned byte")
  (equalp (decode-string-to-ub8 "")         (ub8 ""))
  (equalp (decode-string-to-ub8 "Zg==")     (ub8 "f"))
  (equalp (decode-string-to-ub8 "Zm8=")     (ub8 "fo"))
  (equalp (decode-string-to-ub8 "Zm9v")     (ub8 "foo"))
  (equalp (decode-string-to-ub8 "Zm9vYg==") (ub8 "foob"))
  (equalp (decode-string-to-ub8 "Zm9vYmE=") (ub8 "fooba"))
  (equalp (decode-string-to-ub8 "Zm9vYmFy") (ub8 "foobar"))
  )

(deftests (decode-test-4 :doc "decode vector to unsigned byte")
  (equalp (decode-vector-to-ub8 (vec ""))         (ub8 ""))
  (equalp (decode-vector-to-ub8 (vec "Zg=="))     (ub8 "f"))
  (equalp (decode-vector-to-ub8 (vec "Zm8="))     (ub8 "fo"))
  (equalp (decode-vector-to-ub8 (vec "Zm9v"))     (ub8 "foo"))
  (equalp (decode-vector-to-ub8 (vec "Zm9vYg==")) (ub8 "foob"))
  (equalp (decode-vector-to-ub8 (vec "Zm9vYmE=")) (ub8 "fooba"))
  (equalp (decode-vector-to-ub8 (vec "Zm9vYmFy")) (ub8 "foobar"))
  )

(defun de-flurp (s1 s2)
  (equal s2
    (with-output-to-string (*standard-output*)
      (with-input-from-string (is s1)
        (encode-char-stream is)))))

(defun de-flurp-o (s1 s2)
  (equal s2
    (with-output-to-string (out)
      (with-input-from-string (is s1)
        (encode-char-stream is :output out)))))

(deftests (decode-test-5 :doc "decode char stream")
  (de-flurp ""       "")
  (de-flurp "f"      "Zg==")
  (de-flurp "fo"     "Zm8=")
  (de-flurp "foo"    "Zm9v")
  (de-flurp "foob"   "Zm9vYg==")
  (de-flurp "fooba"  "Zm9vYmE=")
  (de-flurp "foobar" "Zm9vYmFy")
  (de-flurp-o ""       "")
  (de-flurp-o "f"      "Zg==")
  (de-flurp-o "fo"     "Zm8=")
  (de-flurp-o "foo"    "Zm9v")
  (de-flurp-o "foob"   "Zm9vYg==")
  (de-flurp-o "fooba"  "Zm9vYmE=")
  (de-flurp-o "foobar" "Zm9vYmFy")
  )

(deftests (decode-test-6 :doc "ignore whitespace")
  (equal "fizy"    (decode " Z m l 6 e Q = = "))
  (equal "fizzy"   (decode " Z m l 6 e n k = "))
  (equal "fizzzy"  (decode " Z m l 6 e n p 5 "))
  (equal "fizzzzy" (decode " Z m l 6 e n p 6 e Q = = ")))

(deftests (decode-test-7 :doc "give error on broken encoding")
  (got-condition (base64-error) (decode "░▒▓█"))
  (got-condition (base64-error) (decode "()<>{}"))
  ;; @@@ we don't error on malformed input?
  ;; (got-condition (base64-error) (decode "z"))
  ;; (got-condition (base64-error) (decode "zz"))
  ;; (got-condition (base64-error) (decode "zzz"))
  ;; (got-condition (base64-error) (decode "zz="))
  ;; (got-condition (base64-error) (decode "z=="))
  ;; (got-condition (base64-error) (decode "z==="))
  ;; (got-condition (base64-error) (decode "zz==="))
  ;; (got-condition (base64-error) (decode "zzz==="))
  ;; (got-condition (base64-error) (decode "zzz=="))
  ;; (got-condition (base64-error) (decode "z=z"))
  ;; (got-condition (base64-error) (decode "zz=z"))
  ;; (got-condition (base64-error) (decode "zzz=z"))
  ;; (got-condition (base64-error) (decode "z==z"))
  )

#|──────────────────────────────────────────────────────────────────────────┤#
 │ Both
 ╰|#

(defparameter *bunny-size* 32)
(defparameter *bunny*
  #(#x00 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x00 #x00
    #x00 #x00 #x78 #x00 #x00 #x03 #xfc #x01 #xc0 #x0f #xfe #x03
    #xe0 #x1f #xfe #x03 #xe0 #x3f #xef #x07 #x60 #xbc #xd3 #x07
    #x60 #x79 #xa9 #x07 #xa0 #x72 #x55 #x07 #x20 #x6d #x29 #x07
    #xa0 #x6a #x55 #x07 #x20 #x65 #x29 #x07 #xa0 #x6a #x55 #x07
    #x40 #x65 #x2b #x07 #x80 #x6a #x53 #x07 #x00 #xe1 #xa7 #x07
    #x00 #xff #xcf #x07 #x00 #xe1 #xf0 #x07 #x80 #x52 #xe9 #x03
    #xc0 #x50 #xe8 #x03 #xc0 #x5e #xef #x03 #xc0 #xe1 #xf0 #x01
    #x80 #xff #xff #x01 #x80 #xff #xff #x00 #x00 #x0f #xfe #x00
    #x00 #xff #x7f #x00 #x00 #xfe #x3f #x00 #x00 #xfc #x1f #x00
    #x00 #xf8 #x1f #x00 #x00 #xf8 #x3f #x00))

(defun print-bits (bits size)
  (let ((row (/ size 8)))
    (loop :for i :from 0 :below (* size row) :do
      (loop :for bit :from 0 :below 8 :do
        (write-char (if (logbitp bit (aref bits i))
                        (code-char #x2588) ;; #\full_block
                        #\space)))
      (when (zerop (mod (1+ i) row))
        (terpri)))))

;; It's either this nonsense or muck about makeing a gray stream subclass
;; that is a ub8 string that read from arrays.

(defparameter *file-a* nil)
(defparameter *file-b* nil)
(defparameter *file-c* nil)

(defun blurt ()
  (setf *file-a* (format nil "bunny_~d" (random 99999999)))
  (setf *file-b* (format nil "bunny_~d" (random 99999999)))
  (setf *file-c* (format nil "bunny_~d" (random 99999999)))
  (with-open-file (out *file-a* :direction :output
                                :element-type '(unsigned-byte 8))
    (write-sequence *bunny* out)))

(defun unblurt ()
  (dolist (f (list *file-a* *file-b* *file-c*))
    (when (probe-file f)
      (delete-file f))))

(deftests (encode-decode-test-1 :doc "encode and decode ub8 stream"
                         :setup blurt :takedown unblurt)
  (progn
    (with-open-file (in *file-a* :direction :input
                                 :element-type '(unsigned-byte 8))
      (with-open-file (out *file-b* :direction :output
                                    :element-type '(unsigned-byte 8))
        (dbase64:encode-ub8-stream in :output out)))
    (with-open-file (in *file-b* :direction :input 
                                 :element-type '(unsigned-byte 8))
      (with-open-file (out *file-c* :direction :output
                                    :element-type '(unsigned-byte 8))
        (dbase64:decode-ub8-stream in :output out)))
    (let ((bun2 (make-array (length *bunny*)
                          :element-type '(unsigned-byte 8))))
      (with-open-file (in *file-c*
                          :direction :input 
                          :element-type '(unsigned-byte 8))
        (read-sequence bun2 in))
      ;;; (print-bits bun2 *bunny-size*) ;; Unnecessary!
      (equalp *bunny* bun2))))

(defparameter *longer*
  (with-output-to-string (s)
    (loop :repeat 200 :do
      (loop :repeat 3 :do
        (format s "foo the bar ~d " (random 10000)))
      (terpri s))))

(deftests (encode-decode-test-2 :doc "encode decode and columns")
  (equal (length *longer*) (length (decode (encode *longer* :columns 76))))
  (equal *longer* (decode (encode *longer* :columns 76)))
  (every (_ (<= (length _) 67))
         (split-sequence #\newline (dbase64:encode *longer* :columns 67))))


(deftests (dbase64-all)
  encode-test-1
  encode-test-2
  encode-test-3
  encode-test-4
  encode-test-5
  encode-test-6

  decode-test-1
  decode-test-2
  decode-test-3
  decode-test-4
  decode-test-5
  decode-test-6
  decode-test-7

  encode-decode-test-1
  encode-decode-test-2
  )

(defun run ()
  ;; (run-group-name 'dbase64-all :verbose t))
  (run-tests 'dbase64-all))

;; End
