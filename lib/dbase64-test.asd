;;;								-*- Lisp -*-
;;; dbase64-test.asd - System definition for dbase64-test
;;;

(defsystem dbase64-test
  :name               "dbase64-test"
  :description        "Tests for dbase64."
  :version            "0.1.0"
  :author             "Nibby Nebbulous <nibbula -(. @ .)- uucp!gmail.com>"
  :license            "GPL-3.0-only"
  :source-control     :git
  :long-description   "Tests for dbase64."
  :depends-on (:test :dbase64 :dlib)
  :components
  ((:file "dbase64-test")))
