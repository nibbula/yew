;;;
;;; dbase64.lisp - Base64 encoding and decoding.
;;;

(defpackage :dbase64
  (:documentation "Base64 encoding and decoding.
Even though we support encoding and decoding strings, It's recommended to use
the :ub8 (unsigned-byte 8) functions to avoid character encoding issues.")
  (:use :cl :dlib)
  (:export
   #:encode
   #:encode-string
   #:encode-ub8
   #:encode-ub8-to-string
   #:encode-vector-to-string
   #:encode-char-stream
   #:encode-ub8-stream

   #:decode
   #:decode-string
   #:decode-ub8
   #:decode-string-to-ub8
   #:decode-vector-to-ub8
   #:decode-char-stream
   #:decode-ub8-stream

   #:+encoding+
   #:+encoding-bin+
   #:+uri-encoding+
   #:+uri-encoding-bin+

   #:base64-error
   ))
(in-package :dbase64)

(define-condition base64-error (simple-error)
  ()
  (:documentation "An error in base64 dencoding."))

(defun zerror (format &rest args)
  "Lazy way to signal our error."
  (error 'base64-error :format-control format :format-arguments args))

(defun zcerror (format &rest args)
  "Lazy way to signal our error."
  (cerror "Go ahaead anyway."
   'base64-error :format-control format :format-arguments args))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro defconst-char (name value &optional doc)
    "Define a base-char string constant."
    `(progn
       (declaim (type simple-base-string ,name))
       (define-constant ,name
         (let ((s ,value))
           (make-array (length s) :element-type 'base-char :initial-contents s))
         ,@(and doc (list doc)))))

  (defmacro defconst-ub8 (name value &optional doc)
    "Define a (unsigned-byte 8) string constant."
    `(progn
       (declaim (type (vector (unsigned-byte 8)) ,name))
       (define-constant ,name
         (let ((s ,value))
           (make-array (length s) :element-type '(unsigned-byte 8)
                                  :initial-contents (map 'vector #'char-code s)))
         ,@(and doc (list doc))
         'equalp)))

  (defmacro defboth (name value doc)
    (let ((char-name (intern (format nil "+~@:(~a~)+" name)))
          (bin-name (intern (format nil "+~@:(~a-BIN~)+" name))))
      `(progn
         (defconst-char ,char-name ,value ,doc)
         (defconst-ub8 ,bin-name ,value ,doc)))))

(defboth encoding
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
  "The set of lower and uppercase letters, numbers, digits, underscore and dash,
 used for base 64 encoding.")

(defboth uri-encoding
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."
  "The set of lower and uppercase letters, numbers, digits, underscore and dash,
 used for base 64 encoding for URIs.")

(defboth whitespace
  #(#\newline #\space #\tab #\return)
  "The set of whitespace that we can ignore for base 64 encoding.")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro encode-loop (encoding)
    `(let ((in (make-array 3 :element-type 'fixnum))
	   (in-pos 0)
	   (out 0)
	   (pad (1- (length ,encoding))))
       (declare (type (simple-array fixnum (3)) in)
		(type fixnum in-pos out pad))
       (flet ((enc (b) (aref ,encoding b)))
	 (declare (ftype (function (fixnum) t) enc)
		  (inline enc))
	 (loop
	   :until (at-endp)
	   :do
	   (setf (aref in in-pos) (next-byte)
		 in-pos (1+ in-pos))
	   (when (= in-pos 3)
	     (setf out (logior (aref in 2)
			       (the fixnum (ash (aref in 1) 8))
			       (the fixnum (ash (aref in 0) 16))))
	     (put-byte (enc (ldb (byte 6 18) out)))
	     (put-byte (enc (ldb (byte 6 12) out)))
	     (put-byte (enc (ldb (byte 6 6) out)))
	     (put-byte (enc (ldb (byte 6 0) out)))
	     (setf in-pos 0)))
	 (case in-pos
	   ((0 3))
	   (1
	    (setf out (the fixnum (ash (aref in 0) 16)))
	    (put-byte (enc (ldb (byte 6 18) out)))
	    (put-byte (enc (ldb (byte 6 12) out)))
	    (put-byte (enc pad))
	    (put-byte (enc pad)))
	   (2
	    (setf out (logior (the fixnum (ash (aref in 0) 16))
			      (the fixnum (ash (aref in 1) 8))))
	    (put-byte (enc (ldb (byte 6 18) out)))
	    (put-byte (enc (ldb (byte 6 12) out)))
	    (put-byte (enc (ldb (byte 6 6) out)))
	    (put-byte (enc pad)))))))

  (defmacro define-encoder (name &key
                            source-type
                            source-ref
                            source-to-fixnum
                            result-type
                            result-ref
                            result-maker
                            encoding-type
                            encoding-element-type
                            encoding-to-result
                            default-encoding
                            newline)
    `(progn
       (declaim (ftype (function (,source-type &key
                                               (:encoding ,encoding-type)
                                               (:columns fixnum))
			         ,result-type)
		       ,name))
       (defun ,name (src &key (encoding ,default-encoding) (columns 0))
         (declare (type ,source-type src)
	          (type ,encoding-type encoding)
                  (type fixnum columns))
         (let ((from 0)
	       (from-len (length src))
	       (output (,result-maker
                        (multiple-value-bind (q r) (floor (length src) 3)
                          (let ((size (+ (* 4 q) (* 4 (signum r)))))
                            (if (plusp columns)
                                 (+ size (floor size columns))
                                 size)))))
               (column 0)
	       (out-pos 0))
           (declare (type fixnum from from-len out-pos column))
           (flet ((next-byte ()
                    ;; (prog1 (,element-byte src from) (incf from)))
                    (prog1 (,source-to-fixnum (,source-ref src from))
                      (incf from)))
	          (at-endp () (= from from-len))
	          (put-byte (b)
                    (setf (,result-ref output out-pos) (,encoding-to-result b))
                    (incf out-pos)
                    (incf column)
                    (when (and (plusp columns) (= column columns))
                      (setf (,result-ref output out-pos) ,newline
                            column 0)
                      (incf out-pos))))
             (declare (ftype (function (,encoding-element-type) t) put-byte)
	              (inline next-byte at-endp put-byte))
             (encode-loop encoding)
             output))))))

(declaim (inline safe-char-code))
(defun safe-char-code (c)
  (let ((r (char-code c)))
    (when (> r #xff)
      (zcerror "Character code greater than #xff ~x. You would lose data."
               r))
    r))

(define-encoder encode-string
  :source-type            string
  :source-ref             char
  :source-to-fixnum       safe-char-code
  :result-type            (simple-array character (*))
  :result-ref             char
  :result-maker           make-string
  :encoding-type          simple-base-string
  :encoding-element-type  base-char
  :encoding-to-result     values
  :default-encoding       +encoding+
  :newline                #\newline)

(eval-when (:compile-toplevel)
  (defmacro make-u8-array (size)
    `(make-array ,size :element-type '(unsigned-byte 8))))

(declaim (inline safe-byte-value))
(defun safe-byte-value (b)
  (when (> b #xff)
    (zcerror "Character code greater than #xff = #x~x. You would lose data."
             b))
  b)

(define-encoder encode-ub8
  :source-type            (vector (unsigned-byte 8))
  :source-ref             aref
  :source-to-fixnum       values
  :result-type            (vector (unsigned-byte 8))
  :result-ref             aref
  :result-maker           make-u8-array
  :encoding-type          (vector (unsigned-byte 8))
  :encoding-element-type  (unsigned-byte 8)
  :encoding-to-result     values
  :default-encoding       +encoding-bin+
  :newline                #.(char-code #\newline))

(define-encoder encode-ub8-to-string
  :source-type      	  (vector (unsigned-byte 8))
  :source-ref             aref
  :source-to-fixnum       values
  :result-type            (simple-array character (*))
  :result-ref             char
  :result-maker           make-string
  :encoding-type          simple-base-string
  :encoding-element-type  base-char
  :encoding-to-result     values
  :default-encoding       +encoding+
  :newline                #\newline)

(define-encoder encode-vector-to-string
  :source-type      	  (vector (integer 0))
  :source-ref             aref
  :source-to-fixnum       safe-byte-value ;; values
  :result-type            (simple-array character (*))
  :result-ref             char
  :result-maker           make-string
  :encoding-type          simple-base-string
  :encoding-element-type  base-char
  :encoding-to-result     values
  :default-encoding       +encoding+
  :newline                #\newline)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro define-stream-encoder (name &key
                                   source-type
                                   getter
                                   putter
                                   endp
                                   stream-to-fixnum
                                   encoding-type
                                   encoding-element-type
                                   default-encoding
                                   newline)
    `(progn
       (declaim (ftype (function (,source-type &key (:encoding ,encoding-type)
                                               (:output stream)
                                               (:columns fixnum))
			         ,source-type)
		       ,name))
       (defun ,name (src &key (output *standard-output*)
                           (encoding ,default-encoding)
                           (columns 0))
         (declare (type ,source-type src)
	          (type ,encoding-type encoding)
                  (type fixnum columns))
         (let (unread ;; place to store unread chars
               (column 0))
           (declare (ignorable unread)
                    (type fixnum column))
           (flet ((next-byte () (,stream-to-fixnum (,getter src)))
	          (at-endp () (,endp src))
	          (put-byte (b)
                    (,putter b output)
                    (incf column)
                    (when (and (plusp columns) (= column columns))
                      (,putter ,newline output)
                      (setf column 0))))
             (declare (ftype (function (,encoding-element-type) t) put-byte)
	              (inline next-byte at-endp put-byte))
             (encode-loop encoding)
             src))))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro char-endp (src)
    `(not (peek-char nil ,src nil nil))))

(define-stream-encoder encode-char-stream
  :source-type           stream
  :getter                read-char
  :putter                write-char
  :endp                  char-endp
  :stream-to-fixnum      char-code
  :encoding-type         simple-base-string
  :encoding-element-type base-char
  :default-encoding      +encoding+
  :newline               #\newline)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro byte-endp (src)
    `(or (eq unread :eof)
         (eq (setf unread (read-byte ,src nil :eof)) :eof)))
  (defmacro byte-read (src)
    `(or (and unread (prog1 unread (setf unread nil)))
         (prog1 (setf unread (read-byte ,src :eof))
           (when (not (eq unread :eof))
             (setf unread nil))))))

(define-stream-encoder encode-ub8-stream
  :source-type           stream
  :getter                byte-read
  :putter                write-byte
  :endp                  byte-endp
  :stream-to-fixnum      values
  :encoding-type         (vector (unsigned-byte 8))
  :encoding-element-type (unsigned-byte 8)
  :default-encoding      +encoding-bin+
  :newline               #.(char-code #\newline))

(defun encode (source &key (to :string) output (columns 0))
  (values
  (typecase source
    (string
     (case to
       (:string (encode-string source :columns columns))
       ;; (:ub8    (encode-string-to-ub8 source :columns columns))
       (t (zerror "Unsupported encode bass64 string to ~s" to))))
    ((vector (unsigned-byte 8))
     (case to
       (:ub8    (encode-ub8 source :columns columns))
       (:string (encode-ub8-to-string source :columns columns))
       (t (zerror "Unsupported encode bass64 ub8 to ~s" to))))
    ((vector (integer 0))
     (case to
       ;; (:ub8    (encode-ub8 source :columns columns))
       (:string (encode-vector-to-string source :columns columns))
       (t (zerror "Unsupported bass64 vector to ~s" to))))
    (stream
     (when (not (input-stream-p source))
       (zerror "base64 encode stream must be an input stream."))
     (typecase (stream-element-type source)
       (character
        (case to
          (:string
           (encode-char-stream source :output (or output *standard-output*)
                                      :columns columns))
          ;; (:ub8
          ;;  (encode-char-stream-to-ub8-stream source :output output
          ;;                                           :columns columns))
          (t
           (zerror "Unsupported bass64 encode character stream to ~s" to))))
       ((unsigned-byte 8)
        (case to
          ;; (:string
          ;;  (encode-ub8-stchar-stream source :output (or ouput *standard-output*)))
          (:ub8
           (encode-ub8-stream source :output output :columns columns))
          (t
           (zerror "Unsupported bass64 encode ub8 stream to ~s" to))))
       (t
        (zerror "Unsupported bass64 encode stream element type ~s"
               (stream-element-type source)))))
    (t
     (zerror "Unsupported base64 encode source type ~s" (type-of source))))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro decode-loop (encoding whitespace)
    `(let ((in (make-array 4 :element-type 'fixnum))
           (in-pos 0)
           byte
           code)
       (declare (type fixnum in-pos))
       (loop
         :until (at-endp)
         :do
         (setf byte (next-byte))
         (cond
           ((and ignore-whitespace (find byte ,whitespace))
            #| ignore |#)
           ((not (setf code (position byte ,encoding)))
            (cond
              ((and (not ignore-whitespace) (find byte ,whitespace))
               (zerror "Found ~@:c at position ~a when whitespace ~
                        was not ignored." byte count))
              (t
               (zerror "Invalid byte ~x at ~a." byte count))))
           ((eql code (1- (length ,encoding))) ;; =
            ;; @@@ I should probably figure if it's necessary to give errors
            ;; on too short input?
            ;; (ecase in-pos
            ;;   (0 (zerror "Unecessary = at ~a" count))
            ;;   (1 (zerror "crap ~a ~a" in-pos count))
            ;;   (1
            ;;    (put-byte (ash (aref in 0) 2)))
            ;;   (2
            ;;    ;; (put-byte (ash (logand #b1111 (aref in 1)) 4)))
            ;;    (put-byte (logior (ash (aref in 0) 2)
            ;;                      (ash (aref in 1) -4))))
            ;;   (3
            ;;    (put-byte (logior (ash (aref in 1) 4)
            ;;                      (ash (aref in 2) -4)))))
            (return))
           (t
            (setf (aref in in-pos) code)
            (ecase in-pos
              (0
               (incf in-pos))
              (1
               (put-byte (logior (ash (aref in 0) 2)
                                 (ash (aref in 1) -4)))
               (incf in-pos))
              (2
               (put-byte (logior (ash (logand #b1111 (aref in 1)) 4)
                                 (ash (aref in 2) -2)))
               (incf in-pos))
              (3
               (put-byte (logior (ash (logand #b11 (aref in 2)) 6) (aref in 3)))
               (setf in-pos 0))))))
       (finalize)
       output))

  (defmacro define-decoder (name &key
                            source-type
                            result-type
                            result-maker
                            encoding-type
                            ;; encoding-element-type
                            encoding-to-result
                            default-encoding
                            whitespace)
    `(progn
       (declaim (ftype (function (,source-type &key (:encoding ,encoding-type)
                                               (:ignore-whitespace t))
                                 ,result-type)
                       ,name))
       (defun ,name (src &key (encoding ,default-encoding) ignore-whitespace)
         (declare (type ,source-type src)
                  (type ,encoding-type encoding))
         (let ((count 0)
               (out-pos 0)
               (output (,result-maker
                        (let (q r main extra)
                          (setf (values q r) (floor (length src) 4)
                                main (* 3 q)
                                extra (ceiling (* 3 r)))
                          (+ main extra)))))
           (declare (type fixnum count out-pos)
                    (type ,result-type output))
           (flet ((next-byte ()
                    (prog1 (aref src count)
                      (incf count)))
                  (at-endp ()
                    (= count (length src)))
                  (put-byte (b)
                    (setf (aref output out-pos) (,encoding-to-result b))
                    (incf out-pos))
                  (finalize ()
                    (setf output (subseq output 0 out-pos))))
             (declare (ftype (function (fixnum) t) put-byte)
                      (inline next-byte at-endp put-byte))
             (decode-loop encoding ,whitespace)
             output))))))

(define-decoder decode-string
  :source-type           string
  :result-type           string
  :result-maker          make-string
  :encoding-type         simple-base-string
  :encoding-to-result    code-char
  :default-encoding      +encoding+
  :whitespace            +whitespace+)
      
(define-decoder decode-ub8
  :source-type           (vector (unsigned-byte 8))
  :result-type           (vector (unsigned-byte 8))
  :result-maker          make-u8-array
  :encoding-type         (vector (unsigned-byte 8))
  :encoding-to-result    values
  :default-encoding      +encoding-bin+
  :whitespace            +whitespace-bin+)

(define-decoder decode-string-to-ub8
  :source-type           string
  :result-type           (vector (unsigned-byte 8))
  :result-maker          make-u8-array
  :encoding-type         simple-base-string
  :encoding-to-result    values
  :default-encoding      +encoding+
  :whitespace            +whitespace+)

(define-decoder decode-vector-to-ub8
  :source-type           (vector (integer 0))
  :result-type           (vector (unsigned-byte 8))
  :result-maker          make-u8-array
  :encoding-type         (vector (unsigned-byte 8))
  :encoding-to-result    values
  :default-encoding      +encoding-bin+
  :whitespace            +whitespace-bin+)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro define-stream-decoder (name &key
                                   source-type
                                   getter
                                   putter
                                   endp
                                   fixnum-to-stream
                                   encoding-type
                                   default-encoding
                                   whitespace)
    `(progn
       (declaim (ftype (function (,source-type &key (:encoding ,encoding-type)
                                               (:output stream)
                                               (:ignore-whitespace t))
			         ,source-type)
		       ,name))
       (defun ,name (src &key (output *standard-output*)
                           (encoding ,default-encoding)
                           ignore-whitespace)
         (declare (type ,source-type src)
	          (type ,encoding-type encoding))
         (let (unread                   ; place to store unread elements
               (count 0))
           (declare (ignorable unread)
                    (type fixnum count))
           (flet ((next-byte ()
                    ;; (prog1 (,stream-to-fixnum (,getter src))
                    (prog1 (,getter src)
                      (incf count)))
	          (at-endp () (,endp src))
	          (put-byte (b) (,putter (,fixnum-to-stream b) output))
                  (finalize ()))
             (declare (ftype (function (fixnum) t) put-byte)
	              (inline next-byte at-endp put-byte))
             (decode-loop encoding ,whitespace)
             src))))))

(define-stream-decoder decode-char-stream
  :source-type           stream
  :getter                read-char
  :putter                write-char
  :endp                  char-endp
  :fixnum-to-stream      code-char
  :encoding-type         simple-base-string
  :default-encoding      +encoding+
  :whitespace            +whitespace+)

(define-stream-decoder decode-ub8-stream
  :source-type           stream
  :getter                byte-read
  :putter                write-byte
  :endp                  byte-endp
  :fixnum-to-stream      values
  :encoding-type         (vector (unsigned-byte 8))
  :default-encoding      +encoding-bin+
  :whitespace            +whitespace-bin+)

(defun decode (source &key (to :string) output (ignore-whitespace t))
  (values
  (typecase source
    (string
     (case to
       (:string
        (decode-string source :ignore-whitespace ignore-whitespace)) 
       (:ub8
        (decode-string-to-ub8 source :ignore-whitespace ignore-whitespace))
       (t
        (zerror "Unsupported decode base64 string to ~s" to))))
    ((vector (unsigned-byte 8))
     (case to
       (:ub8
        (decode-ub8 source :ignore-whitespace ignore-whitespace))
       ;; (:string
       ;;  (decode-ub8-to-string source :ignore-whitespace ignore-whitespace))
       (t
        (zerror "Unsupported decode base64 ub8 to ~s" to))))
    ((vector (integer 0))
     (case to
       (:ub8
        (decode-vector-to-ub8 source :ignore-whitespace ignore-whitespace))
       ;; (:string
       ;;  (decode-vector-to-string source :ignore-whitespace ignore-whitespace))
       (t
        (zerror "Unsupported decode base64 vector to ~s" to))))
    (stream
     (when (not (input-stream-p source))
       (zerror "base64 decode must be an input stream."))
     (typecase (stream-element-type source)
       (character
        (case to
          (:string
           (decode-char-stream source :output (or output *standard-output*)
                               :ignore-whitespace ignore-whitespace))
          ;; (:ub8
          ;;  (decode-char-stream-to-ub8-stream source :output output))
          (t
           (zerror "Unsupported bass64 decode character stream to ~s" to))))
       ((unsigned-byte 8)
        (case to
          ;; (:string
          ;;  (decode-ub8-stchar-stream source :output (or ouput *standard-output*)))
          (:ub8
           (decode-ub8-stream source :output output
                                     :ignore-whitespace ignore-whitespace))
          (t
           (zerror "Unsupported bass64 decode ub8 stream to ~s" to))))
       (t
        (zerror "Unsupported bass64 decode stream element type ~s"
               (stream-element-type source)))))
    (t
     (zerror "Unsupported base64 decode source type ~s" (type-of source))))))

;; End
