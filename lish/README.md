#### `lish` may someday be a Lisp shell.

If you really want to try it, have sbcl around, and:

```
git clone https://codeberg.org/nibbula/yew.git
cd yew/lish
sh ./build.sh
./lish
```

The help command may be of use.

*Note* that this depends on [yew](https://codeberg.org/nibbula/yew).
